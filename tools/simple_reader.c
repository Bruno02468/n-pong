#include "simple_reader.h"

int max_raw_adc = 17600, last_raw_adc=-1;
double last_pos[8] = {0, 0, 0, 0, 0, 0, 0, 0};
int gpio_pins[8] = {ADC_GPIO_PIN_1, ADC_GPIO_PIN_2, ADC_GPIO_PIN_3, ADC_GPIO_PIN_4, ADC_GPIO_PIN_5, ADC_GPIO_PIN_6, ADC_GPIO_PIN_7, ADC_GPIO_PIN_8};
int i2c_file;

int ler_controle(int controle) {
    if (controle > MAX_CONTROLES || controle < 1) {
        printf("Fatal: tentativa de ler o controle ilegal %d!\n", controle);
        exit(0);
    }
	uint8_t addr=0, pinmode=0, digital_pin=0;
	switch (controle) {
	case 1:
		addr = ADC_A_I2C_ID;
		pinmode = ADC_PINMODE_0_GND;
		digital_pin = ADC_GPIO_PIN_1;
		break;
	case 2:
		addr = ADC_A_I2C_ID;
		pinmode = ADC_PINMODE_1_GND;
		digital_pin = ADC_GPIO_PIN_2;
		break;
	case 3:
		addr = ADC_A_I2C_ID;
		pinmode = ADC_PINMODE_2_GND;
		digital_pin = ADC_GPIO_PIN_3;
		break;
	case 4:
		addr = ADC_A_I2C_ID;
		pinmode = ADC_PINMODE_3_GND;
		digital_pin = ADC_GPIO_PIN_4;
		break;
	case 5:
		addr = ADC_B_I2C_ID;
		pinmode = ADC_PINMODE_0_GND;
		digital_pin = ADC_GPIO_PIN_5;
		break;
	case 6:
		addr = ADC_B_I2C_ID;
		pinmode = ADC_PINMODE_1_GND;
		digital_pin = ADC_GPIO_PIN_6;
		break;
	case 7:
		addr = ADC_B_I2C_ID;
		pinmode = ADC_PINMODE_2_GND;
		digital_pin = ADC_GPIO_PIN_7;
		break;
	case 8:
		addr = ADC_B_I2C_ID;
		pinmode = ADC_PINMODE_3_GND;
		digital_pin = ADC_GPIO_PIN_8;
		break;
	default:
		break;
	}
	// Verify is connector is on
	if (!is_gpio_pin_high(digital_pin)) {
		return -10;
	}
	if (addr != 0) {
		int val = single_shot_read(addr, pinmode, ADC_GAIN_1);
		return val;
	}

}

uint64_t get_time() {
	struct timeval tv;
	gettimeofday(&tv, NULL);
	return 1000000*tv.tv_sec + tv.tv_usec;
}

// Abre o arquivo do i2c para ler os controles; e configura o GPIO
int abre_controles() {
    int addr, err, err2;

	// Ask for GPIO permissions
	err = setgid(997);
	if (err != 0) {
		err2 = errno;
		printf("Failed to get GPIO (gid 997) permission setgid(997)=%d errno=%d.\n", err, err2);
	}

    i2c_file = open("/dev/i2c-1", O_RDWR);
    if (i2c_file < 0) {
        printf("Fatal: Falha ao abrir a interface i2c para ler controles!\n");
        return 1;
    }
    addr = ADC_A_I2C_ID;
    if (ioctl(i2c_file, I2C_SLAVE, addr) < 0) {
        printf("Fatal: Falha ao se conectar com o ADC em 0x%02x!\n", addr);
        return 2;
    }
    addr = ADC_B_I2C_ID;
    if (ioctl(i2c_file, I2C_SLAVE, addr) < 0) {
        printf("Fatal: Falha ao se conectar com o ADC em 0x%02x!\n", addr);
        return 2;
    }

    // Unexport gpio pins
    int gpio_file = open("/sys/class/gpio/unexport", O_WRONLY);
    if (gpio_file == -1) {
        printf("Fatal: Falha ao abrir /sys/class/gpio/unexport!\n");
        return 3;
    }
    for (int i=0; i < 8; i++) {
        char buf[10];
        int pin = gpio_pins[i];
        snprintf(buf, 3, "%d", pin);
        if(write(gpio_file, buf, strlen(buf)) == -1) {
            int err_val = errno;
            printf("Fatal: Falha ao desexportar pino %s para o controle %d! (errno=%d)\n", buf, i+1, err_val);
        }
    }
    close(gpio_file);


    // Export pins
    gpio_file = open("/sys/class/gpio/export", O_WRONLY);
    if (gpio_file == -1) {
        printf("Fatal: Falha ao abrir /sys/class/gpio/export!\n");
        return 4;
    }
    for (int i=0; i < 8; i++) {
        char buf[10];
        int pin = gpio_pins[i];
        snprintf(buf, 3, "%d", pin);
        if(write(gpio_file, buf, strlen(buf)) == -1) {
            int err_val = errno;
            printf("Fatal: Falha ao obter pino %s para o controle %d! (errno=%d)\n", buf, i+1, err_val);
            return 5;
        }
    }
    close(gpio_file);

    for (int i=0; i < 8; i++) {
        gpio_file = 0;
        int pin = gpio_pins[i];
        char path[100];
        snprintf(path, 100, "/sys/class/gpio/gpio%d/direction", pin);
        gpio_file = open(path, O_WRONLY);
        int err_val = errno;
        if (gpio_file == -1) {
            printf("Fatal: Falha ao obter controle sobre a direção do pino %d para o controle %d! (errno=%d)\n", pin, i+1, err_val);
            printf("%s\n", path);
            return 6;
        }
        char buf[3];
        snprintf(buf, 3, "%d", pin);
        char msg[] = "in\0";
        if (write(gpio_file, msg, strlen(msg)) == -1) {
            err_val = errno;
            close(gpio_file);
            printf("Fatal: Falha ao definir o pino %d como entrada para o controle %d! (errno=%d)\n", pin, i+1, err_val);
            printf("%s\n", path);
            return 7;
        }
        close(gpio_file);
    }

    return 0;
}

int single_shot_read(uint8_t addr, uint8_t pin_mode, uint8_t gain) {
    int written_bytes;
    uint8_t msg[3] = {};

    if (ioctl(i2c_file, I2C_SLAVE, addr) < 0) {
        printf("AVISO: Falha ao se conectar com o ADC em 0x%02x!\n", addr);
        return 0.5;
    }

    // Select register
    msg[0] = ADC_CONFIG_REGISTER;
    // Configure the read operation and request it
    msg[1] = ADC_SINGLE_SHOT_START | pin_mode | gain | ADC_CONTINUOUS_MODE;
    msg[2] = ADC_DATA_RATE_860SPS | ADC_COMP_MODE_DEFAULT | ADC_COMP_POLARITY_DEFAULT | ADC_NO_LATCH | ADC_NO_COMPARATOR;
    written_bytes = write(i2c_file, msg, 3);
    if (written_bytes != 3) {
        printf("Aviso: escritos %d ao invés de 3 bytes. Addr = %02x Full message: %02x %02x %02x\n", written_bytes, addr, msg[0], msg[1], msg[2]);
		return -20;
    }
    usleep(ADC_DATA_RATE_860SPS_DELAY);

	int raw_adc = -1;
	// Request data
	msg[0] = ADC_CONVERSION_REGISTER;
	written_bytes = write(i2c_file, msg, 1);
	if (written_bytes != 1) {
		printf("Aviso: escritos %d ao invés de 1 bytes. Addr = %02x Full message: %02x\n", written_bytes, addr, msg[0]);
		return -30;
	}
	
	// Read data
	msg[0] = 0;
	msg[1] = 0;
	msg[2] = 0;
	int read_bytes = read(i2c_file, msg, 2);
	if (read_bytes != 2) {
		printf("Aviso: lidos %d ao invés de 2 bytes. Addr = %02x Read buffer: %02x %02x %02x\n", read_bytes, addr, msg[0], msg[1], msg[2]);
		return -40;
	} else {
		raw_adc = ((int)msg[0] * 256 + msg[1]);
		if (raw_adc > 32767) {
			raw_adc -= 65535;
		}
		if (raw_adc < 0) {
			raw_adc = 0;
		}
		max_raw_adc = (max_raw_adc < raw_adc) ? raw_adc : max_raw_adc;
		return raw_adc;
	}
}

int is_gpio_pin_high(int pin) {
    char path[100];
    char val_str[20];

    snprintf(path, 100, "/sys/class/gpio/gpio%d/value", pin);
    int file = open(path, O_RDONLY);
    if (file == -1) {
        printf("Failed to open gpio pin %d for reading! (errno=%d)\n%s\n", pin, errno, path);
        return 0;
    }
    if (read(file, val_str, 20) == 0) {
        printf("Failed to read gpio pin %d! (errno=%d)\n%s\n", pin, errno, path);
        return 0;
    }
    close(file);
    int val = atoi(val_str);
    // printf("GPIO pin %d = %d\n", pin, val);
    return val;
}

void my_pause() {
	char nada;
	printf("Press any key to continue...");
	scanf("%c", &nada);
}

int main() {
	int err, start, end;

	err = abre_controles();
	if (err != 0) {
		printf("abre_controles retornou %d\n", err);
	}

	int flag = 1;
	while (1) {
		int val[8];
		start = get_time();
		for (int i=0; i < 8; i++) {
			val[i] = ler_controle(i+1)/10;
		}
		end = get_time();
		for (int i=0; i < 8; i++) {
			printf(ANSI_RESET"%d=", i+1);
			if (val[i] >= 0) {
				printf("%s%5d%s", ANSI_BOLD, val[i], ANSI_RESET);
			} else if (val[i] == -1) {
				printf("%s%s%5d%s", ANSI_BOLD, ANSI_YELLOW, val[i], ANSI_RESET);
			} else {
				printf("%s%s%5d%s", ANSI_BOLD, ANSI_RED, val[i], ANSI_RESET);
			}
			printf("   ");
		}
		if (flag > 0) {
			printf("   ");
		} else {
			printf("  *");
		}
		flag *= -1;
		printf(" (%6d µs for all 8 controllers)", end-start);
		printf(ANSI_LINE_START);
	}
	
	return 0;
}
