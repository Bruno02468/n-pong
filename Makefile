# para compilar e rodar o projeto mais facilmente

# constantes, e opções que são reusadas...
CC=cc
CC_FLAGS=-Wall -Wextra --std=c99 -D_GNU_SOURCE -pedantic -Wno-unused-result
CC_LIBS=-lm -pthread -lglut -lGL
ALLEGRO=-lallegro_audio -lallegro_acodec -lallegro
C_SOURCES=src/*.c
PRODUCTION_BINARY=bin/n-pong
DEBUG_BINARY=bin/n-pong_debug
CC_BASE=$(CC) $(CC_FLAGS) $(C_SOURCES) $(CC_LIBS)

# mensagem para explicar como usar o makefile
default:
	@echo "Especifique compile, clean[-debug], run, ou run-sem-controles."
	@echo "Para debug, use debug-gdb, debug-memcheck, ou debug-massif."

.PHONY: clean

# remove o binário normal
clean:
	rm -f $(PRODUCTION_BINARY)

# remove o binário de debug
clean-debug:
	rm -f $(DEBUG_BINARY)

# compila a versão de "produção" do programa (sem símbolos de debug, otimizada)
compile: $(PRODUCTION_BINARY)
$(PRODUCTION_BINARY): src/*.c src/headers/*.h
	$(CC_BASE) -O3 -o $(PRODUCTION_BINARY)

compile-sem-controles:
	$(CC_BASE) -O3 -DDEBUG_SEM_CONTROLES -o $(PRODUCTION_BINARY)

# compila e roda a versão de "produção" do programa
# leitura dos controles requer root, então rodará com sudo
run:
	make clean
	make compile
	sudo $(PRODUCTION_BINARY)

fast-run: $(PRODUCTION_BINARY)
	$(PRODUCTION_BINARY)

fast-run-gdb: $(DEBUG_BINARY)
	sudo gdb $(DEBUG_BINARY)

# mesma coisa, só que sem incluir o código de leitura dos controles
# usado para debugar gráficos e outras coisas fora da plaquinha final
run-sem-controles:
	make clean
	make compile-sem-controles
	$(PRODUCTION_BINARY)

# alias para acima
run-sc:
	make run-sem-controles

# roda o jogo, agora usandok o arquivo do sistema de debug de controles via web
run-web:
	make clean
	make compile-sem-controles
	$(PRODUCTION_BINARY) web_debug/controles_debug.dat


# compila uma versão do programa com símbolos de debug
compile-debug: $(DEBUG_BINARY)
$(DEBUG_BINARY): src/*.c src/headers/*.h
	$(CC_BASE) -g -o $(DEBUG_BINARY)

# idem, sem controles
compile-debug-sc:
	make clean-debug
	$(CC_BASE) -g -DDEBUG_SEM_CONTROLES -o $(DEBUG_BINARY)

# roda o programa dentro do gnu debugger
debug-gdb:
	make compile-debug
	gdb -ex run --args $(DEBUG_BINARY) --debug

# idem, sem controles
debug-gdb-sc:
	make compile-debug-sc
	gdb -ex run --args $(DEBUG_BINARY) --debug

# roda o programa dentro do valgrind (memcheck: procura mau uso de memória)
debug-memcheck:
	make compile-debug
	valgrind --leak-check=full $(DEBUG_BINARY) --debug

# idem, sem controles
debug-memcheck-sc:
	make compile-debug-sc
	valgrind --leak-check=full $(DEBUG_BINARY) --debug

# roda o programa dentro do valgrind (massif: detecta memory leaks)
debug-massif:
	make compile-debug
	valgrind --tool=massif $(DEBUG_BINARY) --debug

# idem, sem controles
debug-massif-sc:
	make compile-debug-sc
	valgrind --tool=massif $(DEBUG_BINARY) --debug
