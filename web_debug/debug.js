// número máxio de controles... idealmente, é uma constante, vai ficar aqui por
// motivos de clareza, apenas
var num_controles = 8;

// elemento onde os sliders serão apendados
var div_controles = document.getElementById("controles");

// endereço do .php que vai receber os valores; mude de acordo com o uso?
var base_php = "setar.php";

// tempo em millisegundos entre cada reenvio dos dados dos controles
// em servidores locais (esperado), esse valor pode ser bem baixo, visto que a
// latência tende a zero...
var periodo_reenvio = 50;

// inicializar os sliders com os rotacionais dos controles
for (var i = 1; i <= num_controles; i++) {
    var html_slider = "Controle " + i + ": <input type=\"range\" id=\"controle-"
        + i + "\" min=\"0\" max=\"1\" step=\"0.01\"><br><br>";
    div_controles.innerHTML += html_slider;
}

// essa função faz a página reenviar os valores dos sliders para o servidor
function reenviar() {
    var valores = [];
    for (var i = 1; i <= num_controles; i++) {
        var slider = document.getElementById("controle-" + i);
        valores.push(slider.value);
    }
    var full_url = base_php + "?valores=" + valores.join(",");
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open("GET", full_url, true);
    xmlHttp.send(null);
}

setInterval(reenviar, periodo_reenvio);
