// implementa análise geométrica da arena do jogo, etc

#include "headers/vetores.h"
#include "headers/estruturas.h"
#include "headers/constantes.h"
#include "headers/geometria.h"
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <time.h>

// acha qual é o jogador dono do setor em que um certo argumento se encontra
int dono_setor(int jogadores, double argumento) {
    argumento = angulo_map(argumento);
    for (int c = 1; c <= jogadores; c++) {
        if ((c * TWO_PI / jogadores) >= argumento) return c-1;
    }
    // isso não é possível!
    printf("Fatal: impossível achar dono do setor em %gº!\n", deg(argumento));
    exit(0);
    return 0;
}

// coloca num intervalo_real o intervalo angular coberto por uma dada raquete
void load_span_raquete(intervalo_real *ir, int jogadores, int raquete,
    double rotacional, bool incluir_tolerancia) {
    double largura_setor = TWO_PI / jogadores;
    double largura_raquete = largura_setor * FRAC_RAQUETE;
    double final_setor = raquete * largura_setor;
    double max_travel = largura_setor - largura_raquete;
    double span_final = final_setor - max_travel * rotacional;
    double tolerancia = TOLERANCIA_RAQUETE * incluir_tolerancia;
    ir->max = span_final + tolerancia;
    ir->min = span_final - largura_raquete - tolerancia;
}

// retorna não-nulo se um número estiver dentro de um intervalo real
bool esta_no_intervalo(double x, const intervalo_real *ir) {
    return (x <= ir->max) && (x >= ir->min);
}

// gera uma double aleatória
double double_aleatoria(double min, double max) {
    srand(time(NULL));
    double range = (max - min);
    double div = RAND_MAX / range;
    return min + (rand() / div);
}

// gera um ângulo aleatório
double angulo_aleatorio() {
    return angulo_map(double_aleatoria(0, TWO_PI));
}

// mapeia um intervalo real para assegurar ângulos, mínimo e máximo (in-place)
void map_ir(intervalo_real *ir) {
    ir->min = angulo_map(ir->min);
    ir->max = angulo_map(ir->max);
    if (ir->min > ir->max) {
        double temp = ir->max;
        ir->max = ir->min;
        ir->min = temp;
    }
}
