// implementação das funções gráficas de alto nível

#include "headers/estruturas.h"
#include "headers/render.h"
#include "headers/constantes.h"
#include "headers/globais.h"
#include "headers/fisica.h"
#include "headers/geometria.h"
#include "headers/vetores.h"
#include "headers/graficos.h"
#include <pthread.h>
#include <GL/glut.h> // garante a inclusão do .h do OpenGL instalado
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>

// essa função pinta na tela a "espera", ou seja, qualquer coisa que role antes
// do jogo em si começar
void desenhar_espera() {
    // FIXME falta isso aqui lel
    if (!es->ja_zerou) {
        // estamos aguardando todos os controles conectados ficarem "low"
        // por enquanto, estou sem ideia do que fazer a espera desenhar aqui
    }
}

// essa função desenha os objetos do jogo em suas respectivas posições
void desenhar_jogo() {
    // desenhar arena e bola
    desenhar_circulo(&vetor_nulo, RAIO_JOGO, &cinza_fraco, false);
    intervalo_real espessura = { RAIO_JOGO, RAIO_JOGO + ESPESSURA_RAQUETE };
    vetor linhas_debug = { 0, RAIO_JOGO, };
    // vetor span_draw = { 0, RAIO_JOGO };
    for (int i = 0; i < gs->jogadores; i++) {
        desenhar_reta(&vetor_nulo, &linhas_debug, &cinza_fraco);
        linhas_debug.argumento += TWO_PI / gs->jogadores;
    }
    intervalo_real span;
    for (int i = 0; i < gs->jogadores; i++) {
        linhas_debug.argumento += TWO_PI / gs->jogadores;
        // desenhar raquetes
        load_span_raquete(&span, gs->jogadores, i+1, gs->raquetes[i].rotacional,
                false);
        int controle_associado = gs->raquetes[i].controle_associado;
        cor *cor_raquete = &cores_raquete[controle_associado-1];
        desenhar_arco_coroa_circular(&vetor_nulo, &span, &espessura, cor_raquete,
            false, true);
    }
    desenhar_circulo(gs->bola->posicional, RAIO_BOLA, &azul, true);
}

// essa função desenha o que deve ser desenhado: no caso, ou a tela de espera,
// ou o jogo em si
void atualizar_tela() {
    pthread_mutex_lock(&lock_estado);
    if (!controles_raw) {
        // se isso acontecer, mano do céu, vai ser PÉSSIMO
        printf("Fatal: gráficos inicializados, controles não!\n");
        exit(0);
    }
    if (!es && !gs) {
        // nem a espera nem o gs são não-nulos, não há nada para desenhar
        // isso não deve acontecer, tá bom?
        printf("Fatal: thread gráfica ativa, mas nada para desenhar!\n");
        exit(0);
    }
    glClearColor(18/255, 18/255, 18/255, 0.0f);
    glClear(GL_COLOR_BUFFER_BIT);
    if (es) {
        // temos que desenhar a espera!
        desenhar_espera();
    } else if (gs) {
        // vamos desenhar o estado atual do jogo então
        desenhar_jogo();
    } else {
        // ?! vamos rezar pra isso nunca acontecer...
        printf("Fatal: nada para desenhar, pior ainda, na linha errada!\n");
        exit(0);
    }
    pthread_mutex_unlock(&lock_estado);
    glFlush();
    glutSwapBuffers();
    glClear(GL_COLOR_BUFFER_BIT);
}

// função chamada a cada frame; redesenha a tela
void timerRedesenhador(int val) {
    // não usaremos o parâmetro, isso silencia o gcc (-Wunused-parameter)
    (void) val;
    glutPostRedisplay();
    glutTimerFunc(1000/FRAMERATE, timerRedesenhador, 0);
}

// callback chamada toda vez que a janela do jogo é redimensionada
void janelaRedimensionada(int w, int h) {
    int maximo = max(w, h);
    int minimo = min(w, h);
    int del = (maximo - minimo) / 2;
    int px, py;
    if (w == maximo) {
        py = 0;
        px = del;
    } else {
        px = 0;
        py = del;
    }
    glViewport(px, py, (GLsizei) minimo, (GLsizei) minimo);
}

// essa thread é a responsável pela atualização dos gráficos da janela
void *thread_grafica(void *dummy) {
    // não usaremos o parâmetro, isso silencia o gcc (-Wunused-parameter)
    (void) dummy;
    glutInit(&global_argc, global_argv);
    glutCreateWindow("n-Pong");
    glutInitWindowSize(WINDOW_SIZE, WINDOW_SIZE);
    glutInitWindowPosition(30, 30);
    glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE);
    graficos_inicializados = true;
    // aplicar as callbacks
    glutDisplayFunc(atualizar_tela);
    glutReshapeFunc(janelaRedimensionada);
    glutTimerFunc(1000/FRAMERATE, timerRedesenhador, 0);
    glutMainLoop();
    return NULL;
}

// essa função cria a janela e inicia a thread gráfica
void init_graficos() {
    if (pthread_create(&id_thread_grafica, NULL, thread_grafica, NULL)) {
        printf("Fatal: Erro ao inicializar a thread de gráficos!\n");
        exit(0);
    }
}
