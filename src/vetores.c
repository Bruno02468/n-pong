// funções para mexer com vetores

#include "headers/estruturas.h"
#include "headers/vetores.h"
#include "headers/geometria.h"
#include "headers/globais.h"
#include "headers/constantes.h"
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

// mapeia um ângulo para o espaço [0, 2pi[
double angulo_map(double a) {
    while (a >= TWO_PI) a -= TWO_PI;
    while (a < 0) a += TWO_PI;
    return a;
}

// cria um novo vetor
vetor *vetor_novo(double argumento, double magnitude) {
    vetor *novo = malloc(sizeof(vetor));
    novo->magnitude = magnitude;
    novo->argumento = angulo_map(argumento);
    return novo;
}

// clona um vetor
vetor *vetor_clonado(const vetor *v) {
    return vetor_novo(v->argumento, v->magnitude);
}

// copia o vetor v para o w
void vetor_copiar(const vetor *v, vetor *w) {
    w->magnitude = v->magnitude;
    w->argumento = v->argumento;
}

// soma dois vetores polares v e w
// a fórmula é meio estranha... a magntiude sai por lei dos cossenos
vetor *vetor_soma(const vetor *v, const vetor *w) {
    double mag1 = v->magnitude;
    double mag2 = w->magnitude;
    double arg1 = v->argumento;
    double arg2 = w->argumento;
    double cosd = cos(arg2-arg1);
    double mag = sqrt(mag1*mag1 + mag2*mag2 + 2*mag1*mag2*cosd);
    double arg = arg1 + atan2(mag2 * sin(arg2-arg1), mag1 + mag2*cosd);
    return vetor_novo(arg, mag);
}

// inverte o sentido de um vetor "in-place"
void vetor_inverter(vetor *v) {
    v->argumento = angulo_map(v->argumento + PI);
}

// rotaciona o vetor "in-place": positivo = sentido horário
void vetor_rotacionar(vetor *v, double a) {
    v->argumento = angulo_map(v->argumento - a);
}

// subtrai dois vetores: retorna v - w
vetor *vetor_diferenca(const vetor *v, const vetor *w) {
    vetor *w_inv = vetor_clonado(w);
    vetor_inverter(w_inv);
    vetor *res = vetor_soma(v, w_inv);
    free(w_inv);
    return res;
}

// multiplica um vetor por um escalar e retorna um novo vetor com o resultado
vetor *vetor_escalonar(const vetor *v, double fator) {
    vetor *res = vetor_clonado(v);
    if (fator < 0) {
        vetor_inverter(res);
        fator = -fator;
    }
    res->magnitude *= fator;
    return res;
}

// converte um vetor para um ponto cartesiano
ponto *vetor_para_ponto(const vetor *v) {
    ponto *p = malloc(sizeof(ponto));
    p->x = v->magnitude * cos(v->argumento);
    p->y = v->magnitude * sin(v->argumento);
    return p;
}

// converte um ponto cartesiano para vetor
vetor *ponto_para_vetor(const ponto *p) {
    double magnitude = sqrt(p->x * p->x + p->y * p->y);
    double argumento = angulo_map(atan2(p->y, p->x));
    return vetor_novo(argumento, magnitude);
}

void printar_vetor(const char *nome, const vetor *v) {
    ponto *cart = vetor_para_ponto(v);
    printf("Debug: vetor \"%s\": (%gº, %gu) ou (%g, %g)\n", nome,
        deg(v->argumento), v->magnitude, cart->x, cart->y);
    free(cart);
}
