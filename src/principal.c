// arquivo principal, contendo o main() e o controle principal de tudo

#include "headers/estruturas.h"
#include "headers/graficos.h"
#include "headers/controles.h"
#include "headers/globais.h"
#include "headers/fisica.h"
#include "headers/geometria.h"
#include "headers/vetores.h"
#include "headers/constantes.h"
#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <stdbool.h>
#include <unistd.h>
#include <sys/types.h>

// thread que lê os controles e coloca no ponteiro global
void *thread_controles(void *dummy) {
    // não usaremos o parâmetro, isso silencia o gcc (-Wunused-parameter)
    (void) dummy;
    for (;;) {
        for (int i = 1; i <= MAX_CONTROLES; i++) {
            double lido = ler_controle(i);
            if (lido > 1) lido = 1;
            else if (lido < 0) lido = 0;
            #ifndef DEBUG_SEM_CONTROLES
            if (!arquivo_controles) lido = 1 - lido;
            #endif
            controles_raw[i-1] = lido;
        }
        congelar(1.0/CONTROL_READ_RATE);
    }
    return NULL;
}

// ponto de entrada do programa
int main(int argc, char **argv) {
    printf("Iniciando n-Pong!\n");
	printf("PID = %d\n", getpid());
    // inicializar as variáveis de globais.h
    init_globais(argc, argv);
    start_audio();

    // inicializar a thread que lê os controles
    if (pthread_create(&id_thread_controles, NULL, thread_controles, NULL)) {
        printf("Fatal: Erro ao inicializar a thread para ler os controles!\n");
        return 0;
    }

    // pronto, agora a thread está lendo os controles
    // agora, incializamos o menu, sem mutex, porque nenhuma outra thread está
    // rodando por enquanto...
    es = malloc(sizeof(espera));
    es->ja_zerou = false;
    es->contagem_ativa = false;
    es->contagem_deu_certo = false;
    es->segundos_restantes = MENU_WAIT;
    es->vai_jogar = malloc(MAX_CONTROLES * sizeof(bool));

    // tudo pronto para os gráficos
    init_graficos();
    pthread_mutex_lock(&lock_menu);

    // só começamos tudo quando dos os controles estiverem low
    while (!es->ja_zerou) {
        pthread_mutex_lock(&lock_estado);
        es->ja_zerou = true;
        for (int i = 0; i < MAX_CONTROLES; i++) {
            if (controles_raw[i] >= ROTACIONAL_MENU) {
                es->ja_zerou = false;
                printf("Debug: aguardando controles ficarem low.\n");
                continue;
            }
        }
        pthread_mutex_unlock(&lock_estado);
        congelar(1);
    }
    printf("Debug: todos os controles estão low.\n");

    // agora começa a contagem!
    es->contagem_ativa = true;
    while (!es->contagem_deu_certo) {
        pthread_mutex_lock(&lock_estado);
        es->quantos = 0;
        for (int i = 0; i < MAX_CONTROLES; i++) {
            es->vai_jogar[i] = controles_raw[i] > ROTACIONAL_MENU;
            // printf("controles_raw[%d] = %lf\n", i, controles_raw[i]);
            es->quantos += es->vai_jogar[i];
        }
        if (es->quantos < 2) {
            // número de jogadores caiu. reiniciar a contagem
            es->contagem_ativa = true;
            es->segundos_restantes = MENU_WAIT;
            printf("Debug: número de jogadores caiu para abaixo de 2. ");
            printf(" Restam %d segundos.\n", es->segundos_restantes);
        } else {
            // número de jogadores continuou acima do mínimo!
            es->contagem_ativa = true;
            es->segundos_restantes--;
            printf("Debug: Número de jogadores está acima do mínimo.");
            printf(" Restam %d segundos.\n", es->segundos_restantes);
            if (!es->segundos_restantes) {
                printf("Debug: Acabou o tempo, jogadores suficientes..\n");
                printf("Debug: vão jogar: ");
                for (int i = 0; i < MAX_CONTROLES; i++) {
                    if (es->vai_jogar[i]) {
                        printf("%d ", (i+1));
                    }
                }
                printf("\n");
                es->contagem_deu_certo = true;
            }
        }
        pthread_mutex_unlock(&lock_estado);
        congelar(1);
    }

    // agora o jogo vai começar. vamos congelar o estado e inicializar o gs.
    pthread_mutex_lock(&lock_estado);
    gs = malloc(sizeof(gamestate));
    gs->raquetes = malloc(es->quantos * sizeof(raquete));
    gs->jogadores = es->quantos;
    int controle = 0;
    for (int i = 0; i < gs->jogadores; i++) {
        while (!es->vai_jogar[controle]) controle++;
        gs->raquetes[i].controle_associado = controle+1;
        gs->raquetes[i].perdas = 0;
        controle++;
    }
    free(es->vai_jogar);
    free(es);
    es = NULL;
    gs->bola = malloc(sizeof(movel));
    gs->bola->velocidade = vetor_novo(angulo_aleatorio(),
        VELOCIDADE_BOLA_INICIAL);
    gs->bola->posicional = vetor_novo(0, 0);
    gs->bola->efeito = 0;
    pthread_mutex_unlock(&lock_estado);

    // o jogo está pronto para a thread física.
    start_soundtrack();
    init_fisica();
    pthread_mutex_lock(&lock_menu);

    printf("Fim da execução: lock de menu soltada.\n");
    return 0;
}
