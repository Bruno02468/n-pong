// implementa as funções de física do jogo

#include "headers/estruturas.h"
#include "headers/globais.h"
#include "headers/vetores.h"
#include "headers/constantes.h"
#include "headers/controles.h"
#include "headers/geometria.h"
#include "headers/graficos.h"
#include "headers/fisica.h"
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <sys/select.h>
#include <time.h>
#include <sys/time.h>
#include <GL/glut.h>

// congelamento na resolução de ns, argumento em segundos, fracionável
void congelar(double sec) {
    int nsec = (sec * 1e9);
    struct timespec delay;
    delay.tv_sec = sec;
    delay.tv_nsec = nsec % (int)1e9;
    //printf("congelando. sec: %g, nsec: %g\n", sec, delay.tv_nsec);
    pselect(0, NULL, NULL, NULL, &delay, NULL);
}

// contar milissegundos desde a era UNIX
time_t timeMillis() {
    struct timeval t;
    gettimeofday(&t, NULL);
    return (t.tv_sec) * 1000 +
        (t.tv_usec) / 1000;
}

// tempo até o qual devemos manter a bola no centro após um gol
time_t wait_bola = 0;

// aplica uma "tick" ao gamestate, movendo os objetos físicos, etc
void *thread_fisica(void *dummy) {
    // não usaremos o parâmetro, isso silencia o gcc (-Wunused-parameter)
    (void) dummy;
    fisica_inicializada = true;
    bool ultimo_foi_oob = false;
    int oob_ticks = 0;
    int play_lose = 0;
    for (;;) {
        play_lose = 0;
        pthread_mutex_lock(&lock_estado);
        // primeiro passo: mover as raquetes
        // FIXME: smoothing?
        for (int r = 0; r < gs->jogadores; r++) {
            int id_controle = gs->raquetes[r].controle_associado-1;
            double lido = controles_raw[id_controle];
            double delta_rotacional = gs->raquetes[r].rotacional - lido;
            gs->raquetes[r].rotacional = lido;
            gs->raquetes[r].rastro *= RASTRO_DECAY;
            double ms = MAX_RASTRO * signum(delta_rotacional);
            gs->raquetes[r].rastro += ms;
            if (fabs(gs->raquetes[r].rastro) > MAX_RASTRO) {
                gs->raquetes[r].rastro = ms;
            }
        }
        // se estivermos esperando a bola, continue esperando
        //if (timeMillis() < wait_bola) {
        //    printf("wtf, %ld < %ld", timeMillis(), wait_bola);
        //    pthread_mutex_unlock(&lock_estado);
        //    continue;
        //}
        // segundo passo: mover a bola
        // primeiro, aplicamos a velocidade de uma tick
        vetor *velocidade_tick = vetor_escalonar(gs->bola->velocidade,
            (double) 1.0/TICKRATE);
        vetor *novo_posicional = vetor_soma(gs->bola->posicional,
            velocidade_tick);
        vetor_copiar(novo_posicional, gs->bola->posicional);
        free(velocidade_tick);
        free(novo_posicional);
        // terceiro passo: verificar se a bola está "out of bounds"
        bool oob = (gs->bola->posicional->magnitude + RAIO_BOLA) >= RAIO_JOGO;
        if (oob) {
            oob_ticks++;
        } else {
            oob_ticks = 0;
        }
        if (oob_ticks >= 5) {
            // algo deu muito errado e a bola ficou muitas ticks para fora da
            // arena do jogo... melhor dar um jeito nisso!
            printf("Excepcional: reposicionando bola (OOB 5ticks+)!\n");
            gs->bola->velocidade->argumento = angulo_aleatorio();
            gs->bola->posicional->magnitude = VELOCIDADE_BOLA_INICIAL;
        }

        // quarto passo: se estiver "out of bounds", verificar se vai bater numa
        // raquete, ou se deve contar como derrota para o dono do setor.
        if (oob && !ultimo_foi_oob) {
            double toque = gs->bola->posicional->argumento;
            int dona = dono_setor(gs->jogadores, toque);
            // int ca = gs->raquetes[dona].controle_associado;
            double rot = gs->raquetes[dona].rotacional;
            intervalo_real span;
            load_span_raquete(&span, gs->jogadores, dona+1, rot, true);
            if (esta_no_intervalo(toque, &span)) {
                play_sound_win();
                // a bola bateu na raquete, então refletimos
                double arg_pos = gs->bola->posicional->argumento;
                double arg_v = gs->bola->velocidade->argumento;
                double arg_nova_v = PI + 2 * arg_pos - arg_v;
                gs->bola->velocidade->argumento = arg_nova_v;
                // agora, o "efeito"
                // primeiro, somamos uma fração do efeito da bola ao da raquete
                double novo_efeito = gs->bola->efeito * PERDA_EFEITO
                    + gs->raquetes[dona].rastro;
                // limitar o efeito a ser aplicado na bola
                novo_efeito = min(novo_efeito, MAX_RASTRO);
                novo_efeito = max(novo_efeito, -MAX_RASTRO);
                int sig = -signum(novo_efeito);
                // limitamos o ângulo normal para evitar "fugas"
                double normal = PI - gs->bola->velocidade->argumento
                    + gs->bola->posicional->argumento + novo_efeito;
                while (fabs(sin(normal)) > fabs(sin(MAX_NORMAL_DEFESA))) {
                    normal = PI - gs->bola->velocidade->argumento
                        + gs->bola->posicional->argumento + novo_efeito;
                    novo_efeito += STEP_REDUC_NORMAL * sig;
                }
                // agora, aplicamos esse desvio ao vetor velocidade da bola
                vetor_rotacionar(gs->bola->velocidade, novo_efeito);
                // salvamos o novo efeito na bola, para valer na próxima batida
                gs->bola->efeito = novo_efeito * TRANSFER_EFEITO;
                // e aceleramos a bola, só um pouquinho
                double incremento = (VELOCIDADE_BOLA_MAXIMA
                    - VELOCIDADE_BOLA_INICIAL) / INCREMENTOS_VELOCIDADE_BOLA;
                gs->bola->velocidade->magnitude = min(incremento
                    + gs->bola->velocidade->magnitude, VELOCIDADE_BOLA_MAXIMA);
            } else {
                // foi gol
                gs->raquetes[dona].perdas++;
                // reposicionar a bola
                gs->bola->posicional->magnitude = 0;
                // retirar quaisquer efeitos
                gs->bola->efeito = 0;
                // aleatorizar a velocidade
                gs->bola->velocidade->magnitude = VELOCIDADE_BOLA_INICIAL;
                gs->bola->velocidade->argumento = angulo_aleatorio();
                play_lose = 1;
            }
        }
        ultimo_foi_oob = oob;
        pthread_mutex_unlock(&lock_estado);
        if (play_lose) {
            // toque o som e espere
            play_sound_lose();
        }
        congelar(1.0/TICKRATE);
    }
    return NULL;
}

void init_fisica() {
    if (pthread_create(&id_thread_fisica, NULL, thread_fisica, NULL)) {
        printf("Fatal: Erro ao inicializar a thread de física!\n");
        exit(0);
    }
}
