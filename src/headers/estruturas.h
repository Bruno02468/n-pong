// contém estruturas usadas para guardar dados relativos ao jogo
// as estruturas são typedef-adas para manter o código mais limpo

#pragma once
#include <stdbool.h>

// vetor: vetor2 na forma polar
typedef struct {
    // ângulo relativo ao sentido horizontal-direito, radianos
    double argumento;
    // magnitude do vetor em seja quais forem as unidades
    double magnitude;
} vetor;

// ponto: ponto no plano cartesiano
typedef struct {
    double x, y;
} ponto;

// raquete: guarda informações sobre o arco que o jogador usa
typedef struct {
    // número identificando o controle associado a essa raquete
    int controle_associado;
    // double de 0 a 1 contendo a última rotação dessa raquete
    double rotacional;
    // gols que essa raquete tomou
    int perdas;
    // desvio angular usado para calcular efeitos
    double rastro;
} raquete;

// movel: define um objeto móvel, como uma bola, que pode adquirir efeito
typedef struct {
    // posição do centro do móvel relativa à origem
    vetor *posicional;
    // velocidade instantânea associada ao móvel
    vetor *velocidade;
    // último efeito obtido, a ser somado aos subsequentes
    double efeito;
} movel;

// define um estado instantâneo do jogo, todo o necessário para desenhá-lo
typedef struct {
    // número de raquetes jogando
    int jogadores;
    // lista das raquetes jogando
    raquete *raquetes;
    // bola em uso
    movel *bola;
} gamestate;

// intervalo real... no conceito matemático mesmo.
typedef struct {
    double min;
    double max;
} intervalo_real;

// define uma estrutura com todas as informações para desenhar a tela de espera]
typedef struct {
    // todos ficaram = 0 concomitantemente?
    bool ja_zerou;
    // contagem regressiva ativa?
    bool contagem_ativa;
    // contagem deu certo?
    bool contagem_deu_certo;
    // segundos restantes na contagem
    int segundos_restantes;
    // vai jogar ou não? indexada por raquetes
    bool *vai_jogar;
    // quantos vão jogar?
    int quantos;
} espera;

// guarda uma cor em RGB
typedef struct {
    double r, g, b;
} cor;
