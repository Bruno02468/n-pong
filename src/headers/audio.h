#pragma once
#include <stdio.h>
#ifdef SOM
#include <allegro5/allegro.h>
#include <allegro5/allegro_audio.h>
#include <allegro5/allegro_acodec.h>


extern ALLEGRO_SAMPLE *aWin, *aLose, *aLoop;
extern int use_audio;
#endif

int start_audio();
void start_soundtrack();
void play_sound_win();
void play_sound_lose();
void close_audio();