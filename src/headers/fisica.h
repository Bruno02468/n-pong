// expõe a inicialização da thread física, e o congelamento

#pragma once
#include <time.h>
#include "audio.h"

void congelar(double sec);
time_t timeMillis();
void init_fisica();
