#pragma once
// interface de controles
// implementação de leitura a ser feita pelo Gabriel

#include <stdint.h>
#include <string.h>
#include <errno.h>

// números mágicos do gabriel
// muitos são dados empíricos a respeito da performance dos pinos e ADCs, visando
// maximizar a performance sem danificar os componentes...

#define ADC_A_I2C_ID 0x4A
#define ADC_B_I2C_ID 0x48

#define ADC_SINGLE_SHOT_START (1 << 7)
#define ADC_PINMODE_0_GND (4 << 4) //0b100
#define ADC_PINMODE_1_GND (5 << 4) //0b101
#define ADC_PINMODE_2_GND (6 << 4) //0b110
#define ADC_PINMODE_3_GND (7 << 4) //0b111
#define ADC_GAIN_1 (0 << 1) // 0b000 ±6.144 V
#define ADC_GAIN_2 (1 << 1) // 0b001 ±4.096 V
#define ADC_GAIN_3 (2 << 1) // 0b010 ±2.048 V (default)
#define ADC_GAIN_4 (3 << 1) // 0b011 ±1.024 V
#define ADC_GAIN_5 (4 << 1) // 0b100 ±0.512 V
#define ADC_GAIN_6 (5 << 1) // 0b101 ±0.256 V
#define ADC_GAIN_7 (6 << 1) // 0b110 ±0.256 V
#define ADC_GAIN_8 (7 << 1) // 0b111 ±0.256 V
#define ADC_SINGLE_SHOT_MODE 1
#define ADC_CONTINUOUS_MODE 0
// SPS = Samples Per Second
#define ADC_DATA_RATE_008SPS (0 << 5) //0b000
#define ADC_DATA_RATE_016SPS (1 << 5) //0b001
#define ADC_DATA_RATE_032SPS (2 << 5) //0b010
#define ADC_DATA_RATE_064SPS (3 << 5) //0b011
#define ADC_DATA_RATE_128SPS (4 << 5) //0b100
#define ADC_DATA_RATE_128SPS_DELAY 20000 // Empirically determined
#define ADC_DATA_RATE_250SPS (5 << 5) //0b101
#define ADC_DATA_RATE_250SPS_DELAY 10240 // (128*ADC_DATA_RATE_128SPS_DELAY)/250
#define ADC_DATA_RATE_475SPS (6 << 5) //0b110
#define ADC_DATA_RATE_475SPS_DELAY 5389 // (128*ADC_DATA_RATE_128SPS_DELAY)/475
#define ADC_DATA_RATE_860SPS (7 << 5) //0b111
// Empirically adjusted. Original value: (128*ADC_DATA_RATE_128SPS_DELAY)/860=2977
#define ADC_DATA_RATE_860SPS_DELAY 2000
#define ADC_COMP_MODE_DEFAULT (0 << 4)
#define ADC_COMP_POLARITY_DEFAULT (0 << 3)
#define ADC_NO_LATCH (0 << 2)
#define ADC_NO_COMPARATOR 3 //0b11
#define ADC_CONVERSION_REGISTER 0
#define ADC_CONFIG_REGISTER 1

#define ADC_GPIO_PIN_1 6
#define ADC_GPIO_PIN_2 13
#define ADC_GPIO_PIN_3 19
#define ADC_GPIO_PIN_4 26
#define ADC_GPIO_PIN_5 12
#define ADC_GPIO_PIN_6 5
#define ADC_GPIO_PIN_7 16
#define ADC_GPIO_PIN_8 20


// garante-se: int controle sempre de 1 a MAX_CONTROLES (constantes.h)
// retorna double entre 0 e 1, 0 sendo rotação máxima no sentido anti-horário,
// e 1 sendo rotação máxima no sentido horário
double ler_controle(int controle);

double single_shot_read(uint8_t addr, uint8_t pin_mode, uint8_t gain);
int abre_controles();
int is_gpio_pin_high(int digital_pin);
