// usado para definir constantes..ué

#pragma once

// meu deus
#undef PI
#undef TWO_PI
#define PI 3.14159265358979323846
#define TWO_PI 2.0 * 3.14159265358979323846

// é sério que o C não tem isso?
#define min(a, b) (((a) < (b)) ? (a) : (b))
#define max(a, b) (((a) > (b)) ? (a) : (b))
#define signum(x) ((x > 0) ? 1 : ((x < 0) ? -1 : 0))
#define deg(rad) (rad*180.0/PI)
#define rad(deg) (deg*PI/180.0)

// base para as demais frequẽncias
#define FREQUENCIA_BASE 30

// frequência da escala de tempo da física do jogo, em Hz
#define TICKRATE (2 * FREQUENCIA_BASE)

// FPS máximo do renderizador, em Hz
#define FRAMERATE (1 * FREQUENCIA_BASE)

// frequência de leitura dos controles, em Hz
#define CONTROL_READ_RATE (1 * FREQUENCIA_BASE)

// número máximo de controles para suportar
// IDs de controle sempre estão entre 1 e esse número
#define MAX_CONTROLES 8

// número de controles no debug
#define CONTROLES_NO_DEBUG 8

// tempo de espera para leitura dos controles até iniciar o jogo, em segundos
#define MENU_WAIT 3

// tempo entre um gol e a bola ser lançada de novo
#define GOL_WAIT 1

// gols máximos que um jogador pode tomar antes de ser eliminado
// experimental, pode cair em desuso ou ser removido por motivo de design
#define MAX_PERDAS 5

// maior valor do rotacional de cada controle para separar entre quem quer jogar
// e quem não quer, e também pra iniciar a contagem
#define ROTACIONAL_MENU 0.25

// raio do jogo, em cujos limites ficam as raquetes
#define RAIO_JOGO 750.0

// tamanho desejado de janela
#define WINDOW_SIZE 512

// raio da bola, em função do raio do jogo
#define RAIO_BOLA (RAIO_JOGO/20.0)

// fração de um setor ocupado pela raquete
#define FRAC_RAQUETE 0.25

// define um ângulo "a mais", que apesar se não ser visível enquanto parte da
// raquete, ainda pode refletir a bola, ou seja, uma margem de tolerância que
// torna o jogo mais realista e menos frustrante
#define TOLERANCIA_RAQUETE rad(3)

// define uma margem para forçar um controle a retornar um rotacional extremo
// exemplo: 0.00000000000000001 vira 0.01
#define LIMITE_ROTACIONAL 0.01

// desvio angular máximo, em módulo, criado por efeitos, em radianos
#define MAX_RASTRO rad(20)

// fração do rastro mantido a cada tick
#define RASTRO_DECAY 0.99

// efeito perdido a cada batida
#define PERDA_EFEITO 0.5

// fração do efeito que é aplicado na bola a cada batida
#define TRANSFER_EFEITO 0.7

// máximo ângulo de reflexão
#define MAX_NORMAL_DEFESA rad(85) - MAX_RASTRO

// "passo" de redução do ângulo de defesa
#define STEP_REDUC_NORMAL rad(5)

// espessura da raquete
#define ESPESSURA_RAQUETE RAIO_JOGO/40

// velocidade da bola, em segundos^-1, mínima e máxima, pois ela aumenta um
// pouco a cada batida, para deixar o jogo um pouco mais desafiador
#define VELOCIDADE_BOLA_INICIAL RAIO_JOGO/2
#define VELOCIDADE_BOLA_MAXIMA  2.0 * VELOCIDADE_BOLA_INICIAL

// número de defesas necessário para que a bola atinja sua velocidade máxima
#define INCREMENTOS_VELOCIDADE_BOLA 5

// tamanho do jogo que deve "sobrar"
#define SOBRA_JOGO 1.1

// escala de desenho
#define ESCALA 0.75

// como o OpenGL não possui facilidades para desenhar círculos e curvas,
// desenhar círculos requer um método iterativo que quebra o círculo em partes
// quanto maior esse valor, maior o número de iterações e a qualidade do círculo
#define ITER_CIRCULAR 30

// número de iterações usadas para preencher algum objeto
#define ITER_PREENCHER 100

// comprimento do ">" na seta de um vetor, e seu ângulo
#define ASA_VETOR RAIO_BOLA
#define ANGULO_ASA rad(30)
