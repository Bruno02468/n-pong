// variáveis globais! isso definitivamente é uma boa ideia!
// zoas, aqui guardamos variáveis de estado que todas as threads compartilham

#pragma once
#include <pthread.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include "estruturas.h"
#include "controles.h"
#include "constantes.h"

// função em globais.c que inicializa a maioria das variáveis declaradas abaixo
void init_globais(int argc, char **argv);

// todas as threads
pthread_t id_thread_grafica, id_thread_controles, id_thread_fisica,
    id_thread_redesenhadora;

// gráficos e física foram inicializados?
bool graficos_inicializados, fisica_inicializada, pausar;

// variáveis de estado do jogo; a mutex se aplica a ambos
gamestate *gs;
espera *es;
pthread_mutex_t lock_estado, lock_menu, lock_controles_debug;
double *controles_raw;

// deixar argc e argv globais
int global_argc;
char **global_argv;

// cores comuns para uso...
cor branco, vermelho, verde, azul, preto, cinza_fraco, rosa, laranja;

// cores padrão de cada raquete
cor cores_raquete[MAX_CONTROLES];

// constantes matemáticas
vetor vetor_nulo;
intervalo_real circulo_cheio;

// usadas na interface dos pinos
int i2c_file, i2c_use;

// arquivo de "controles de debug"
FILE *arquivo_controles;
