// expõe funções gráficas de baixo nível mas nem tudo que está no render.c

#pragma once
#include "estruturas.h"

void desenhar_arco(const vetor *centro, double raio, const intervalo_real
    *angulos, const cor *fill);
void desenhar_circulo(const vetor *centro, double raio, const cor *fill, bool
    preencher);
void desenhar_arco_coroa_circular(const vetor *centro, const intervalo_real
    *largura, const intervalo_real *espessura, const cor *fill, bool preencher,
    bool bordas);
void desenhar_texto(const vetor *centro, double proporcional, const char *texto,
    const cor *fill);
void desenhar_reta(const vetor *v, const vetor *w, const cor *fill);
void desenhar_vetor(const vetor *v, const vetor *inicio, const cor *fill, double
    escala);
