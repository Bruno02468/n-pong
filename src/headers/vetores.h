// interface das funções em vetores.c

#pragma once
#include "estruturas.h"

double angulo_map(double a);
vetor *vetor_novo(double argumento, double magnitude);
vetor *vetor_clonado(const vetor *v);
void vetor_copiar(const vetor *v, vetor *w);
vetor *vetor_soma(const vetor *v, const vetor *w);
void vetor_inverter(vetor *v);
void vetor_rotacionar(vetor *v, double a);
vetor *vetor_diferenca(const vetor *v, const vetor *w);
vetor *vetor_escalonar(const vetor *v, double fator);
ponto *vetor_para_ponto(const vetor *v);
vetor *ponto_para_vetor(const ponto *p);
void printar_vetor(const char *nome, const vetor *v);
