// interface das funções em geometria.c

#pragma once
#include "estruturas.h"

int dono_setor(int jogadores, double argumento);
void load_span_raquete(intervalo_real *ir, int jogadores, int raquete,
    double rotacional, bool incluir_tolerancia);
bool esta_no_intervalo(double x, const intervalo_real *ir);
double double_aleatoria(double min, double max);
double angulo_aleatorio();
void map_ir(intervalo_real *ir);
