// inicializa as variáveis globais (listadas em globais.h)

#include "headers/estruturas.h"
#include "headers/constantes.h"
#include "headers/globais.h"
#include <pthread.h>
#include <stdlib.h>
#include <stdio.h>

// os objetos de cor do OpenGL usam valores float de 0 a 1, mas como sabemos que
// cores RGB são três valores entre 0 e 255, vamos tornar o processo mais
// eficiente usando structs com essas frações pré-computadas
// essa função guarda na struct cor apontada os valores certos para uma cor RGB
void init_cor_rgb(cor *ptr, int r, int g, int b) {
    ptr->r = r/255.0;
    ptr->g = g/255.0;
    ptr->b = b/255.0;
}

// essa função inicializa as variáveis globais: mutexes, cores padrão, tudo
void init_globais(int argc, char **argv) {
    // booleans de estado, ponteitos, mutexes: coisas do funcionamento interno
    // também externamos o argc e o argv, que normalmente seriam invisíveis
    // fora da thread principal (que contém o main())
    graficos_inicializados = fisica_inicializada = false;
    pthread_mutex_init(&lock_estado, NULL);
    pthread_mutex_init(&lock_menu, NULL);
    pthread_mutex_init(&lock_controles_debug, NULL);
    gs = NULL;
    es = NULL;
    controles_raw = NULL;
    global_argc = argc;
    global_argv = argv;

    // agora inicializamos umas cores padrão, também usadas no debug
    branco.r = branco.g = branco.b = 1.0;
    preto.r = preto.g = preto.b = 0.0;
    azul.r = azul.g = verde.r = verde.b = vermelho.g = vermelho.b = 0.0;
    azul.b = verde.g = vermelho.r = 1.0;
    cinza_fraco.r = cinza_fraco.g = cinza_fraco.b = 0.2;
    init_cor_rgb(&rosa, 254, 127, 156);
    init_cor_rgb(&laranja, 255, 117, 26);

    // agora, as cores "hardcoded" de correspondência entre player e raquete
    init_cor_rgb(&(cores_raquete[0]), 255, 12, 12);
    init_cor_rgb(&(cores_raquete[1]), 28, 255, 35);
    init_cor_rgb(&(cores_raquete[2]), 255, 110, 0);
    init_cor_rgb(&(cores_raquete[3]), 0, 250, 255);
    init_cor_rgb(&(cores_raquete[4]), 255, 50, 200);
    init_cor_rgb(&(cores_raquete[5]), 255, 242, 0);
    init_cor_rgb(&(cores_raquete[6]), 42, 137, 50);
    init_cor_rgb(&(cores_raquete[7]), 102, 46, 145);

    // enfim, umas structs matemáticas que usamos várias vezes
    circulo_cheio.min = 0;
    circulo_cheio.max = TWO_PI;
    vetor_nulo.argumento = vetor_nulo.magnitude = 0;

    // inicializamos a interface de leitura dos pinos, e a array
    // onde os valores lidos ficam guardados
    controles_raw = calloc(MAX_CONTROLES, sizeof(double));

    // agora, existe a possibilidade de ter sido passado um arquivo com a saída
    // dos controles no terminal... nesse caso, leremos esse arquivo!
    if (argc > 1) {
        arquivo_controles = fopen(argv[1], "r");
        if (!arquivo_controles) {
            printf("Fatal: O arquivo de debug \"%s\" não existe!\n", argv[1]);
            exit(0);
        }
    }


    #ifndef DEBUG_SEM_CONTROLES
    int abriu_ok = abre_controles();
    if (abriu_ok) {
        printf("Fatal: abre_controles() retornou %d.\n", abriu_ok);
        exit(0);
    }
    #endif
}
