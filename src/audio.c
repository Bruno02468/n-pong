#ifdef SOM
#include "headers/audio.h"

ALLEGRO_SAMPLE *aWin = NULL;
ALLEGRO_SAMPLE *aLose = NULL;
ALLEGRO_SAMPLE *aLoop = NULL;
ALLEGRO_SAMPLE_ID *iLoop = NULL;
int use_audio=0;
unsigned long int lWin, lLose;

int start_audio() {
    // Init audio
    if(!al_init()) {
        printf("Failed to initialize allegro!\n");
        return 1;
    }
    if(!al_install_audio()) {
        printf("Failed to initialize audio!\n");
        return 2;
    }
    if(!al_init_acodec_addon()) {
        printf("Failed to initialize audio codecs!\n");
        return 3;
    }
    if(!al_reserve_samples(3)) {
        printf("Failed to reserve samples!\n");
        return 4;
    }
    use_audio = 1;

    // Load sound effects
    aWin = al_load_sample("win.wav");
    if(aWin == NULL) {
        printf("Failed to load win sound effect!\n");
        return 5;
    }
    aLose = al_load_sample("lose.wav");
    if(aLose == NULL) {
        printf("Failed to load lose sound effect!\n");
        return 6;
    }
    aLoop = al_load_sample("battle_loop.ogg");
    if(aLose == NULL) {
        printf("Failed to load battle loop!\n");
        return 7;
    }

    // Calculate audio lengths
    ALLEGRO_SAMPLE_INSTANCE  *instance;
    instance = al_create_sample_instance(aWin);
    lWin = 1E6 * al_get_sample_instance_time(instance);
    instance = al_create_sample_instance(aLose);
    lLose = 1E6 * al_get_sample_instance_time(instance);

    return 0;
}

void start_soundtrack() {
    if (use_audio && aWin != NULL) {
        printf("Starting sound track\n");
        al_play_sample(aLoop, 1.0, 0.0, 1.0, ALLEGRO_PLAYMODE_LOOP, iLoop);
    }
}

void play_sound_win() {
    if (use_audio && aWin != NULL) {
        printf("Playing win sound\n");
        al_play_sample(aWin, 1.0, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
    }
}

void play_sound_lose() {
    if (use_audio && aLose != NULL) {
        printf("Playing lose sound\n");
        al_play_sample(aLose, 1.0, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
        usleep(lLose);
    }
}

void close_audio() {
    //Free the sound effects
    al_destroy_sample(aWin);
    al_destroy_sample(aLose);
    al_destroy_sample(aLoop);
}
#else
int start_audio() { return 0; }
void start_soundtrack() {}
void play_sound_win() {}
void play_sound_lose() {}
void close_audio() {}
#endif