// implementa as funções gráficas de baixo nível, chamadas no graficos.c

#include "headers/estruturas.h"
#include "headers/constantes.h"
#include "headers/globais.h"
#include "headers/geometria.h"
#include "headers/vetores.h"
#include "headers/render.h"
#include <pthread.h>
#include <GL/glut.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

// aplica a escala de desenho a uma coordenada
double E(double x) {
    return x / RAIO_JOGO * ESCALA;
}

// aplica um ponto como vértice
void vertex_ponto(const ponto *p) {
    glVertex2d(E(p->x), E(p->y));
}

// aplica um vetor como vértice
void vertex_vetor(const vetor *v) {
    ponto *p = vetor_para_ponto(v);
    vertex_ponto(p);
    free(p);
}

// aplica uma struct cor ao contexto opengl
void set_cor(const cor *fill) {
    glColor3d(fill->r, fill->g, fill->b);
}

// fecha o programa com uma mensagem de erro caso uma função gráfica seja
// chamada antes da inicialização do contexto gráfico
void fatal_nao_inicializou() {
    printf("Fatal: tentativa de atualizar os gráficos antes da inicialização ");
    printf("do contexto OpenGL!\n");
    exit(0);
}

// desenha um arco na tela... bem autoexplicativo, sabe
void desenhar_arco(const vetor *centro, double raio, const intervalo_real
    *angulos, const cor *fill) {
    if (!graficos_inicializados) fatal_nao_inicializou();
    // pré-computamos o ângulo de cada iteração, juntamente com seu cos e tan
    double angulo_iter = angulo_map((angulos->max - angulos->min)
        / (ITER_CIRCULAR-1.0));
    double cosseno_iter = cos(angulo_iter);
    double tangente_iter = tan(angulo_iter);
    // como o jogo trabalha mais com posições na forma vetorial, as funções
    // gráficas vão se encarregar de fazer a conversão para cartesianas
    ponto *pc = vetor_para_ponto(centro);
    //printf("ponto centro: %g, %g\n", pc->x, pc->y);
    ponto ultimo_ponto = { raio * cos(angulos->min), raio * sin(angulos->min) };
    glBegin(GL_LINE_STRIP);
    set_cor(fill);
    for (int i = 0; i < ITER_CIRCULAR; i++) {
        glVertex2d(E(ultimo_ponto.x + pc->x), E(ultimo_ponto.y + pc->y));

        // agora vamos para o próximo ponto
        double ultimo_x = -ultimo_ponto.y;
        double ultimo_y = ultimo_ponto.x;

        ultimo_ponto.x += ultimo_x * tangente_iter;
        ultimo_ponto.y += ultimo_y * tangente_iter;
        ultimo_ponto.x *= cosseno_iter;
        ultimo_ponto.y *= cosseno_iter;
        // sim, isso é uma rotação através de uma matriz de transformação
    }
    //glVertex2d(E(retornar.x), E(retornar.y));
    glEnd();
    free(pc);
    // as funções de baixo nível NÃO chamam glFlush, isso é papel das funções
    // de alto nível, pois várias renderizações são feitas por vez
}

// círculos são só arcos completos!
void desenhar_circulo(const vetor *centro, double raio, const cor *fill, bool
    preencher) {
    intervalo_real total = { 0, raio };
    desenhar_arco_coroa_circular(centro, &circulo_cheio, &total, fill,
    preencher, false);
}

// desenha um arco de coroa circular na tela
void desenhar_arco_coroa_circular(const vetor *centro, const intervalo_real
    *largura, const intervalo_real *espessura, const cor *fill, bool preencher,
    bool bordas) {
    // primeiro, desenhamos os arcos!
    desenhar_arco(centro, espessura->min, largura, fill);
    desenhar_arco(centro, espessura->max, largura, fill);
    // agora, as linhas conectando o arco menor ao maior
    glBegin(GL_LINES);
    set_cor(fill);
    vetor temp = { largura->min, espessura->min };
    if (preencher) {
        double step = (largura->max - largura->min)
            / (preencher ? ITER_PREENCHER : 1);
        for (temp.argumento = largura->min; temp.argumento <= largura->max;
        temp.argumento += step) {
            temp.magnitude = espessura->min;
            vetor *s = vetor_soma(&temp, centro);
            vertex_vetor(s);
            free(s);
            temp.magnitude = espessura->max;
            vetor *p = vetor_soma(&temp, centro);
            vertex_vetor(p);
            free(p);
        }
    } else if (bordas) {
        vertex_vetor(&temp);
        temp.magnitude = espessura->max;
        vertex_vetor(&temp);
        temp.argumento = largura->max;
        vertex_vetor(&temp);
        temp.magnitude = espessura->min;
        vertex_vetor(&temp);
    }
    glEnd();
}

// desenha uma reta, simples.
void desenhar_reta(const vetor *v, const vetor *w, const cor *fill) {
    glBegin(GL_LINES);
    set_cor(fill);
    vertex_vetor(v);
    vertex_vetor(w);
    glEnd();
}

// representa um vetor na tela (usado para debug)
void desenhar_vetor(const vetor *v, const vetor *inicio, const cor *fill, double
    escala) {
    vetor *tv = vetor_escalonar(v, escala);
    vetor *ponta = vetor_soma(tv, inicio);
    desenhar_reta(inicio, ponta, fill);
    vetor *asa = vetor_clonado(tv);
    free(tv);
    asa->argumento += ANGULO_ASA;
    asa->magnitude = ASA_VETOR;
    vetor *p = vetor_soma(asa, ponta);
    desenhar_reta(ponta, p, fill);
    free(p);
    asa->argumento -= 2 * ANGULO_ASA;
    vetor *q = vetor_soma(asa, ponta);
    desenhar_reta(ponta, q, fill);
    free(q);
    free(ponta);
    free(asa);
}
