// implementa as funções que leem os controles via GPIO

#include "headers/controles.h"
#include "headers/estruturas.h"
#include "headers/constantes.h"
#include "headers/fisica.h"
#include "headers/globais.h"
#include <math.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <unistd.h>
#ifndef DEBUG_SEM_CONTROLES
    #include <linux/i2c-dev.h>
    #include <sys/ioctl.h>
    #include <fcntl.h>
#endif

int max_raw_adc = 17600;
double last_pos[8] = {0, 0, 0, 0, 0, 0, 0, 0};
int gpio_pins[8] = {
    ADC_GPIO_PIN_1, ADC_GPIO_PIN_2, ADC_GPIO_PIN_3, ADC_GPIO_PIN_4,
    ADC_GPIO_PIN_5, ADC_GPIO_PIN_6, ADC_GPIO_PIN_7, ADC_GPIO_PIN_8
};

double ler_controle(int controle) {
    if (controle > MAX_CONTROLES || controle < 1) {
        printf("Fatal: tentativa de ler o controle ilegal %d!\n", controle);
        exit(0);
    }
    #ifndef DEBUG_SEM_CONTROLES
    if (i2c_use == 1) {
        uint8_t addr=0, pinmode=0, digital_pin=0;
        switch (controle) {
        case 1:
            addr = ADC_A_I2C_ID;
            pinmode = ADC_PINMODE_0_GND;
            digital_pin = ADC_GPIO_PIN_1;
            break;
        case 2:
            addr = ADC_A_I2C_ID;
            pinmode = ADC_PINMODE_1_GND;
            digital_pin = ADC_GPIO_PIN_2;
            break;
        case 3:
            addr = ADC_A_I2C_ID;
            pinmode = ADC_PINMODE_2_GND;
            digital_pin = ADC_GPIO_PIN_3;
            break;
        case 4:
            addr = ADC_A_I2C_ID;
            pinmode = ADC_PINMODE_3_GND;
            digital_pin = ADC_GPIO_PIN_4;
            break;
        case 5:
            addr = ADC_B_I2C_ID;
            pinmode = ADC_PINMODE_0_GND;
            digital_pin = ADC_GPIO_PIN_5;
            break;
        case 6:
            addr = ADC_B_I2C_ID;
            pinmode = ADC_PINMODE_1_GND;
            digital_pin = ADC_GPIO_PIN_6;
            break;
        case 7:
            addr = ADC_B_I2C_ID;
            pinmode = ADC_PINMODE_2_GND;
            digital_pin = ADC_GPIO_PIN_7;
            break;
        case 8:
            addr = ADC_B_I2C_ID;
            pinmode = ADC_PINMODE_3_GND;
            digital_pin = ADC_GPIO_PIN_8;
            break;
        default:
            break;
        }
        // Verify is connector is on
        if (!is_gpio_pin_high(digital_pin)) {
            return 0;
        }
        if (addr != 0) {
            double val = single_shot_read(addr, pinmode, ADC_GAIN_1);
            last_pos[controle-1] = val;
            return val;
        }
    }
    #endif
    if (arquivo_controles) {
        // parece que foi fornecido um arquivo com os controles de debug...
        pthread_mutex_lock(&lock_controles_debug);
        rewind(arquivo_controles);
        int controle_atual = 1;
        char char_lido;
        while (controle_atual < controle) {
            do {
                char_lido = fgetc(arquivo_controles);
                if (char_lido == EOF) {
                    // printf("Aviso: EOF encontrado ao ler controle %d!\n",
                    //     controle);
                    // encontramos um EOF, isso significa que demos azar e
                    // pegamos o arquivo numa reescrita... melhor voltar pro
                    // começo e tentar de novo!
                    rewind(arquivo_controles);
                    controle_atual = 1;
                }
            } while (char_lido != '\n');
            controle_atual++;
        }
        double rotacional_lido;
        fscanf(arquivo_controles, "%lf", &rotacional_lido);
        pthread_mutex_unlock(&lock_controles_debug);
        return rotacional_lido;
    }
    if (es) {
        if (es->ja_zerou) {
            // para debugar com menos controles
            // if (controle > 2) return 0;
            #ifdef DEBUG_SEM_CONTROLES
            if (controle > CONTROLES_NO_DEBUG) return 0.0;
            return 1.0;
            #else
            return 0.0;
            #endif
        } else {
            return 0.0;
        }
    } else {
        double p = (cos(cos(controle) * timeMillis() / 1000) + 1) / 2;
        if (p < LIMITE_ROTACIONAL) return 0;
        else if (p > (1 - LIMITE_ROTACIONAL)) return 1;
        else return p;
    }
}

#ifndef DEBUG_SEM_CONTROLES
// Abre o arquivo do i2c para ler os controles; e configura o GPIO
int abre_controles() {
    int addr, err, err2;

	// Ask for GPIO permissions
	err = setgid(997);
	if (err != 0) {
		err2 = errno;
		printf("Failed to get GPIO (gid 997) permission setgid(997)=%d"
            " errno=%d.\n", err, err2);
	}

	i2c_use = 0;
    i2c_file = open("/dev/i2c-1", O_RDWR);
    if (i2c_file < 0) {
        printf("Fatal: Falha ao abrir a interface i2c para ler controles!\n");
        return 1;
    }
    addr = ADC_A_I2C_ID;
    if (ioctl(i2c_file, I2C_SLAVE, addr) < 0) {
        printf("Fatal: Falha ao se conectar com o ADC em 0x%02x!\n", addr);
        return 2;
    }
    addr = ADC_B_I2C_ID;
    if (ioctl(i2c_file, I2C_SLAVE, addr) < 0) {
        printf("Fatal: Falha ao se conectar com o ADC em 0x%02x!\n", addr);
        return 2;
    }
    i2c_use = 1;

    // Unexport gpio pins
    int gpio_file = open("/sys/class/gpio/unexport", O_WRONLY);
    if (gpio_file == -1) {
        printf("Fatal: Falha ao abrir /sys/class/gpio/unexport!\n");
        return 3;
    }
    for (int i=0; i < 8; i++) {
        char buf[10];
        int pin = gpio_pins[i];
        snprintf(buf, 3, "%d", pin);
        if(write(gpio_file, buf, strlen(buf)) == -1) {
            int err_val = errno;
            printf("Fatal: Falha ao desexportar pino %s para o controle %d! "
                "(errno=%d)\n", buf, i+1, err_val);
        }
    }
    close(gpio_file);


    // Export pins
    gpio_file = open("/sys/class/gpio/export", O_WRONLY);
    if (gpio_file == -1) {
        printf("Fatal: Falha ao abrir /sys/class/gpio/export!\n");
        return 4;
    }
    for (int i=0; i < 8; i++) {
        char buf[10];
        int pin = gpio_pins[i];
        snprintf(buf, 3, "%d", pin);
        if(write(gpio_file, buf, strlen(buf)) == -1) {
            int err_val = errno;
            printf("Fatal: Falha ao obter pino %s para o controle %d! "
                "(errno=%d)\n", buf, i+1, err_val);
            return 5;
        }
    }
    close(gpio_file);

    for (int i=0; i < 8; i++) {
        gpio_file = 0;
        int pin = gpio_pins[i];
        char path[100];
        snprintf(path, 100, "/sys/class/gpio/gpio%d/direction", pin);
        gpio_file = open(path, O_WRONLY);
        int err_val = errno;
        if (gpio_file == -1) {
            printf("Fatal: Falha ao obter controle sobre a direção do pino %d "
                "para o controle %d! (errno=%d)\n", pin, i+1, err_val);
            printf("%s\n", path);
            return 6;
        }
        char buf[3];
        snprintf(buf, 3, "%d", pin);
        char msg[] = "in\0";
        if (write(gpio_file, msg, strlen(msg)) == -1) {
            err_val = errno;
            close(gpio_file);
            printf("Fatal: Falha ao definir o pino %d como entrada para o "
                "controle %d! (errno=%d)\n", pin, i+1, err_val);
            printf("%s\n", path);
            return 7;
        }
        close(gpio_file);
    }

    return 0;
}

void read_debug() {
    uint8_t buf[10];
    int n = read(i2c_file, buf, 10);
    printf("Got %d bytes:", n);
    for (int i=0; i < n && buf[i] != 255; i++) {
        printf(" 0x%02X", buf[i]);
    }
    printf("\n");
}

double single_shot_read(uint8_t addr, uint8_t pin_mode, uint8_t gain) {
    int written_bytes;
    uint8_t msg[3] = {0, 0, 0};

    if (ioctl(i2c_file, I2C_SLAVE, addr) < 0) {
        printf("AVISO: Falha ao se conectar com o ADC em 0x%02x!\n", addr);
        return 0.5;
    }

    // Select register
    msg[0] = ADC_CONFIG_REGISTER;
    // Configure the read operation and request it
    msg[1] = ADC_SINGLE_SHOT_START | pin_mode | gain | ADC_CONTINUOUS_MODE;
    msg[2] = ADC_DATA_RATE_860SPS | ADC_COMP_MODE_DEFAULT
        | ADC_COMP_POLARITY_DEFAULT | ADC_NO_LATCH | ADC_NO_COMPARATOR;
    written_bytes = write(i2c_file, msg, 3);
    if (written_bytes != 3) {
        printf("Aviso: escritos %d ao invés de 3 bytes. Addr = %02x "
            "Full message: %02x %02x %02x\n", written_bytes, addr, msg[0],
            msg[1], msg[2]);
    }
    usleep(ADC_DATA_RATE_860SPS_DELAY);

    // Request data
    msg[0] = ADC_CONVERSION_REGISTER;
    written_bytes = write(i2c_file, msg, 1);
    if (written_bytes != 1) {
        printf("Aviso: escritos %d ao invés de 1 bytes. Addr = %02x "
            "Full message: %02x\n", written_bytes, addr, msg[0]);
    }

    // Read data
    msg[0] = 0;
    msg[1] = 0;
    msg[2] = 0;
    int read_bytes = read(i2c_file, msg, 2);
    if (read_bytes != 2) {
        printf("Aviso: lidos %d ao invés de 2 bytes. Addr = %02x Read buffer: "
            "%02x %02x %02x\n", read_bytes, addr, msg[0], msg[1], msg[2]);
        return 0;
    } else {
        int raw_adc = ((int)msg[0] * 256 + msg[1]);
        if (raw_adc > 32767) {
            raw_adc -= 65535;
        }
        if (raw_adc < 0) {
            raw_adc = 0;
        }
        max_raw_adc = (max_raw_adc < raw_adc) ? raw_adc : max_raw_adc;
        double ans = ((double) raw_adc)/((double) max_raw_adc);
        if (raw_adc <= 0) {
            return -1;
        }
        return ans;
    }
}

int is_gpio_pin_high(int pin) {
    char path[100];
    char val_str[20];

    snprintf(path, 100, "/sys/class/gpio/gpio%d/value", pin);
    int file = open(path, O_RDONLY);
    if (file == -1) {
        printf("Failed to open gpio pin %d for reading! (errno=%d)\n%s\n", pin,
            errno, path);
        return 0;
    }
    if (read(file, val_str, 20) == 0) {
        printf("Failed to read gpio pin %d! (errno=%d)\n%s\n", pin, errno,
            path);
        return 0;
    }
    close(file);
    int val = atoi(val_str);
    return val;
}

#endif
