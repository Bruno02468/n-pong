ESTRUTURA DO CÓDIGO:

    instruções de verdade ficam em arquivos .c com nomes descritivos em src/
    definições sem instruções (constantes, estruturas, etc) em src/headers/

    se algo na arquitetura abaixo parecer arbitrário, veja o constantes.h
    provavelmente é arbitrário mesmo, mas agora você sabe o valor!


FUNCIONAMENTO DO PROGRAMA:

    1) menu aparece esperando todos os controles serem girados no sentido AH
    2) menu manda girar no sentido H controles que querem jogar
    3) começa uma contagem quando temos dois jogadores dispostos
       (e ela para caso esse número caia para menos de dois)
    4) se a contagem terminar, o jogo começa
    5) toda vez que a bola atravessa o seu pedaço do arco, você perde uma vida
    6) quando perder todas as vidas, é eliminado (os arcos mudam)
    7) quando só sobrar um, ele é declarado vencedor e o programa reinicia
       (controles são identificados por cor, tanto fisicamente quanto na tela)


O QUE SERÁ USADO:

    compilador: GCC, standard C99 (mais detalhes no Makefile)
    build: GNU Make
    debug: GDB e valgrind (memcheck e massif)
    bibliotecas relevantes:
        - posix threads (libpthread)
        - math.h (libmath)
        - OpenGL (via FreeGLUT e libglew, para gráficos)

MÚSICAS:

  - http://freemusicarchive.org/music/Visager/Songs_From_An_Unmade_World_2/Visager_-_Songs_From_An_Unmade_World_2_-_22_Battle_-Loop-
